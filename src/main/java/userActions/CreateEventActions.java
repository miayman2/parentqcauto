package userActions;

import constants.ConstantParameters;
import data.EventData;
import data.UserData;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import screens.CreateEventScreen;
import screens.HomeScreen;
import screens.LandingScreen;
import screens.LoginScreen;

import java.util.HashMap;

public class CreateEventActions extends ConstantParameters {

    private HomeScreen homeScreen;
    private CreateEventScreen createEventScreen;

    public CreateEventActions(WebDriver driver){
        homeScreen = new HomeScreen(driver);
        createEventScreen = new CreateEventScreen(driver);
    }

    public HashMap<String, Object> navigateToCreateEventForm(String requiredInstitute) throws InterruptedException {
        HashMap<String, Object> navigationResult = new HashMap<>();

        if(!homeScreen.navigateToRequiredInstitute(requiredInstitute)) {
            navigationResult.put(IS_ACTION_SUCCESS_KEY, false);
            navigationResult.put(ERROR_MESSAGE_KEY, MESSAGE_WHEN_INSTITUTE_NOT_OPEN);
            return navigationResult;
        }

        if(!homeScreen.navigateToCalendarTab()){
            navigationResult.put(IS_ACTION_SUCCESS_KEY, false);
            navigationResult.put(ERROR_MESSAGE_KEY, MESSAGE_WHEN_CALENDAR_SCREEN_NOT_LOADING);
            return navigationResult;
        }

        if(!homeScreen.navigateToCreateEvent()){
            navigationResult.put(IS_ACTION_SUCCESS_KEY, false);
            navigationResult.put(ERROR_MESSAGE_KEY, MESSAGE_WHEN_CREATE_EVENT_NOT_OPEN);
            return navigationResult;
        }

        navigationResult.put(IS_ACTION_SUCCESS_KEY, true);
        navigationResult.put(ERROR_MESSAGE_KEY, EMPTY);
        return navigationResult;
    }

    public HashMap<String, Object> fillCreateEventForm(EventData eventData,
                                               boolean isMandatoryParamsOnly) throws InterruptedException {

        HashMap<String, Object> creationResults = new HashMap<>();

        createEventScreen.setupEventTitle(eventData.eventTitle);

        if(!createEventScreen.setupEventRecipients(eventData.eventRecipients)){
            creationResults.put(IS_ACTION_SUCCESS_KEY, false);
            creationResults.put(ERROR_MESSAGE_KEY, MESSAGE_WHEN_RECIPIENT_NOT_FOUND);
            return creationResults;
        }

        if(!createEventScreen.setupEventDate(eventData.eventDate)){
            creationResults.put(IS_ACTION_SUCCESS_KEY, false);
            creationResults.put(ERROR_MESSAGE_KEY, MESSAGE_WHEN_DATE_NOT_VALID);
            return creationResults;
        }

        createEventScreen.setupEventStartTime(eventData.eventTimeStart);
        createEventScreen.setupEventEndTime(eventData.eventTimeEnd);
        if(!isMandatoryParamsOnly) {
            createEventScreen.setupEventDescription(eventData.eventDescription);
            
            createEventScreen.setReservationNeeded(eventData.isReservationNeeded);

            if (eventData.isReservationNeeded) {
                if(!createEventScreen.setEventForList(eventData.eventFor)){
                    creationResults.put(IS_ACTION_SUCCESS_KEY, false);
                    creationResults.put(ERROR_MESSAGE_KEY, MESSAGE_WHEN_EVENT_FOR_NOT_FOUND);
                    return creationResults;
                }
                if(!createEventScreen.setupReplyDeadlineDate(eventData.replyDeadline)){
                    creationResults.put(IS_ACTION_SUCCESS_KEY, false);
                    creationResults.put(ERROR_MESSAGE_KEY, MESSAGE_WHEN_DATE_NOT_VALID);
                    return creationResults;
                }
            }

            createEventScreen.setRemindMeBefore(eventData.isReminderNeeded);
            createEventScreen.uploadImageToTheEvent();
        }

        creationResults.put(IS_ACTION_SUCCESS_KEY, true);
        creationResults.put(ERROR_MESSAGE_KEY, EMPTY);
        return creationResults;
    }

    public String submitCreatedEvent(boolean isSuccessSubmit) throws InterruptedException {

        if(isSuccessSubmit){
            return createEventScreen.submitCreateEventForm();
        }else{
            //TODO change return here to ValidationMessages.getText or failureToasterMessage.
            return null;
        }
    }


}
