package userActions;

import constants.ConstantParameters;
import data.UserData;
import org.openqa.selenium.WebDriver;
import screens.HomeScreen;
import screens.LandingScreen;
import screens.LoginScreen;

public class UserLoginActions extends ConstantParameters {

    private HomeScreen homeScreen;
    private LoginScreen loginScreen;

    public UserLoginActions(WebDriver driver){
        homeScreen = new HomeScreen(driver);
        loginScreen = new LoginScreen(driver);
    }

    public boolean login(String email, String password, String usedBrowserName,
                         boolean isNegativeScenario) throws InterruptedException {

        loginScreen.setupLoginEmail(email);
        loginScreen.setupLoginPassword(password);
        if(!isNegativeScenario){
            boolean loginResult = loginScreen.submitLoginRequest();
            if(usedBrowserName.equals(FIREFOX_DRIVER_NAME) && loginResult){
                homeScreen.cancelAlertOfNotification();
            }
            return loginResult;
        } else {
            //TODO return validationMessages.isDisplayed instead.
            return !loginScreen.submitLoginRequest();
        }
    }
}
