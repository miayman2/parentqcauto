package screens;

import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.util.ArrayList;
import java.util.List;

public class CreateEventScreen extends BaseScreen{

    @FindBy(id = CREATE_EVENT_SCREEN_EVENT_TITLE_TEXT_FIELD_ID)
    private WebElement eventTitleTextField;

    @FindBy(id = CREATE_EVENT_SCREEN_EVENT_DESCRIPTION_TEXT_AREA_ID)
    private WebElement eventDescriptionTextField;

    @FindBy(css = CREATE_EVENT_SCREEN_RECIPIENTS_FIELD_CSS)
    private WebElement eventRecipientsField;

    @FindBy(css = CREATE_EVENT_SCREEN_LIST_OPTIONS_CSS)
    private List<WebElement> eventRecipientsMenuOptions;

    @FindBy(css = CREATE_EVENT_SCREEN_EVENT_DATE_FIELD_CSS)
    private List<WebElement> eventDateField;

    @FindBy(id = CREATE_EVENT_SCREEN_EVENT_START_TIME_FIELD_ID)
    private WebElement eventStartTimeField;

    @FindBy(id = CREATE_EVENT_SCREEN_EVENT_END_TIME_FIELD_ID)
    private WebElement eventEndTimeField;

    @FindBy(id = CREATE_EVENT_SCREEN_RESERVATION_NEEDED_BUTTON_ID)
    private WebElement reservationNeededButton;

    @FindBy(id = CREATE_EVENT_SCREEN_EVENT_FOR_ID)
    private WebElement eventForList;

    @FindBy(css = CREATE_EVENT_SCREEN_LIST_OPTIONS_CSS)
    private List<WebElement> eventForOptions;

    @FindBy(id = CREATE_EVENT_SCREEN_REPLY_DEADLINE_FIELD_ID)
    private WebElement replyDeadlineDate;

    @FindBy(id = CREATE_EVENT_SCREEN_REMIND_ME_BEFORE_BUTTON_ID)
    private WebElement remindMeBefore;

    @FindBy(className = CREATE_EVENT_SCREEN_DATE_CALENDAR_PICKER_CLASS)
    private WebElement datePicker;

    @FindBy(css = CREATE_EVENT_SCREEN_UPLOAD_IMAGE_BUTTON)
    private WebElement uploadImageButton;

    @FindBy(id = CREATE_EVENT_SCREEN_UPLOAD_IMAGE_INPUT_FIELD)
    private WebElement uploadImageInputField;

    @FindBy(id = CREATE_EVENT_SCREEN_SUBMIT_EVENT_BUTTON_ID)
    private WebElement submitCreateEventButton;

    @FindBy(id = HOME_SCREEN_CREATE_EVENT_SUCCESS_TOASTER)
    private WebElement successToaster;


    public CreateEventScreen(WebDriver driver) {
        super(driver);
    }

    public void setupEventTitle(String eventTitle){
        eventTitleTextField.clear();
        eventTitleTextField.sendKeys(eventTitle);
    }

    public void setupEventDescription(String eventDescription){
        eventDescriptionTextField.clear();
        eventDescriptionTextField.sendKeys(eventDescription);
    }

    public boolean setupEventRecipients(ArrayList<String> recipientsData) throws InterruptedException {
        int optionsIdx = 0;
        int numberOfOptions = 0;


        for(String recipientData : recipientsData){

            eventRecipientsField.click();
            webDriverWait.until(ExpectedConditions.visibilityOf(eventRecipientsMenuOptions.get(0)));
            Thread.sleep(THREE_HUNDRED_MS);


            eventRecipientsField.findElement(By.tagName(INPUT_ELEMENT_TAG_NAME))
                    .sendKeys(recipientData.split(START_OF_STUFF_CHILDREN_PART_RECIPIENTS_FIELDS)[0]);

            for(int listLoadWaitIdx = 0; listLoadWaitIdx < 10; listLoadWaitIdx++){
                Thread.sleep(FIVE_HUNDRED_MS);

                try {
                    numberOfOptions = eventRecipientsMenuOptions.size();
                    optionsIdx = 0;

                    for (WebElement recipientOption : eventRecipientsMenuOptions) {
                        if (recipientOption.getText().contains(recipientData)) {
                            recipientOption.click();
                            break;
                        } else {
                            optionsIdx++;
                        }
                    }
                }catch (StaleElementReferenceException staleElementReferenceException){
                    continue;
                }
                if(optionsIdx < numberOfOptions){
                    break;
                }
            }
            if(optionsIdx == numberOfOptions){
                return false;
            }
        }
        return true;
    }

    public boolean setupEventDate(String eventDate) throws InterruptedException {
        eventDateField.get(0).click();
        webDriverWait.until(ExpectedConditions.visibilityOf(datePicker));
        Thread.sleep(THREE_HUNDRED_MS);

        return setDateField(eventDate);
    }

    public void setupEventStartTime(String eventStartTime){
        setTimeField(eventStartTimeField, eventStartTime);
    }

    public void setupEventEndTime(String eventEndTime){
        setTimeField(eventEndTimeField, eventEndTime);
    }

    public void setReservationNeeded(boolean isReservationNeeded) throws InterruptedException {
        boolean isButtonAlreadyChecked = Boolean.parseBoolean(reservationNeededButton.getAttribute("checked"));
        if(isButtonAlreadyChecked != isReservationNeeded) {
            jsDriver.executeScript("arguments[0].click()", reservationNeededButton);
            jsDriver.executeScript("window.scrollBy(0, 1000);");
            if(isReservationNeeded){
                webDriverWait.until(ExpectedConditions.visibilityOf(eventForList));
            }else{
                webDriverWait.until(ExpectedConditions.invisibilityOf(eventForList));
            }
            Thread.sleep(THREE_HUNDRED_MS);
        }
    }

    public boolean setEventForList(String optionToBeSelected) throws InterruptedException {
        int optionsIdx = 0;
        int numberOfOptions;

        eventForList.click();
        webDriverWait.until(ExpectedConditions.visibilityOf(eventForOptions.get(0)));
        Thread.sleep(FIVE_HUNDRED_MS);

        numberOfOptions = eventForOptions.size();

        for (WebElement eventForOption : eventForOptions) {
            if(eventForOption.getText().contains(optionToBeSelected)){
                eventForOption.click();
                break;
            } else {
                optionsIdx++;
            }
        }

        return optionsIdx != numberOfOptions;
    }

    public boolean setupReplyDeadlineDate(String deadlineDate) throws InterruptedException {
        eventDateField.get(1).click();
        webDriverWait.until(ExpectedConditions.visibilityOf(datePicker));
        Thread.sleep(THREE_HUNDRED_MS);

        return setDateField(deadlineDate);
    }

    public void setRemindMeBefore(boolean isRemindBeforeNeeded){
        boolean isButtonAlreadyChecked = Boolean.parseBoolean(remindMeBefore.getAttribute(CHECKED_ATTRIBUTE_NAME));
        if(isButtonAlreadyChecked != isRemindBeforeNeeded) {
            jsDriver.executeScript("arguments[0].click()", remindMeBefore);
        }
    }

    public void uploadImageToTheEvent() throws InterruptedException {
        uploadImageButton.click();
        Thread.sleep(THREE_HUNDRED_MS);
        uploadImageInputField.sendKeys(getResourceFileAbsolutePathFromFileName(EVENT_IMAGE_FILE_NAME));
        webDriverWait.until(ExpectedConditions.presenceOfElementLocated(By.id(CREATE_EVENT_SCREEN_UPLOADED_IMAGE_PREVIEW_ID)));
        webDriverWait.until(ExpectedConditions.attributeContains(By.id(CREATE_EVENT_SCREEN_UPLOADED_IMAGE_PREVIEW_ID),WEB_ELEMENT_CLASS_NAME,"loaded"));
        Thread.sleep(THREE_HUNDRED_MS);
    }

    public String submitCreateEventForm() throws InterruptedException {
        submitCreateEventButton.click();

        try {
            webDriverWait.until(ExpectedConditions.urlContains("calendar"));
            webDriverWait.until(ExpectedConditions.visibilityOf(successToaster));
            Thread.sleep(THREE_HUNDRED_MS);

            return successToaster.getText();

        } catch (TimeoutException | NoSuchElementException ex){
            return null;
        }
    }

}
