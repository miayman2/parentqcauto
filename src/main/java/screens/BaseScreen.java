package screens;

import constants.ConstantParameters;
import constants.ElementsLocators;
import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.concurrent.TimeUnit;

public class BaseScreen extends ElementsLocators {

    public WebDriver driver;
    public WebDriverWait webDriverWait;
    public JavascriptExecutor jsDriver;

    @FindBy(className = CREATE_EVENT_SCREEN_DATE_MONTH_AND_YEAR_CLASS)
    private List<WebElement> monthAndYearButton;

    @FindBy(css = CREATE_EVENT_SCREEN_DATE_CALENDAR_CELL_CSS)
    private List<WebElement> calendarCells;

    @FindBy(id = "onesignal-slidedown-cancel-button")
    private WebElement alertCancelButton;



    public BaseScreen(WebDriver driver) {
        driver.manage().timeouts().implicitlyWait(ONE_SECONDS_IN_MS, TimeUnit.MILLISECONDS);

        PageFactory.initElements(driver, this);

        this.driver = driver;
        webDriverWait = new WebDriverWait(driver, WAIT_TIME_OUT_IN_SECONDS);
        jsDriver = (JavascriptExecutor) driver;

    }

    public void navigateToPortal(){
        driver.get(LANDING_SCREEN_URL);
    }

    public void setTimeField(WebElement fieldToSet, String timeToSetInField){
        String eventHour;
        boolean isMorning;
        if(timeToSetInField.contains(MORNING_PART_IN_TIME_FIELDS)){
            eventHour = timeToSetInField.split(MORNING_PART_IN_TIME_FIELDS)[0];
            isMorning = true;
        } else {
            eventHour = timeToSetInField.split(EVENING_PART_IN_TIME_FIELDS)[0];
            isMorning = false;
        }
        fieldToSet.clear();
        fieldToSet.sendKeys(eventHour);
        if(!isMorning){
            fieldToSet.sendKeys(Keys.BACK_SPACE);
            fieldToSet.sendKeys(Keys.BACK_SPACE);
            fieldToSet.sendKeys(EVENING_PART_IN_TIME_FIELDS);
        }
        fieldToSet.click();
    }

    public boolean setDateField(String eventDate) throws InterruptedException {
        if(LocalDate.parse(eventDate, DateTimeFormatter.ofPattern("dd MMMM yyyy")).isBefore(LocalDate.now())){
            return false;
        }

        int cellsIdx = 0;
        String dayValue = eventDate.split(" ")[0].charAt(0) == '0'?
                eventDate.split(" ")[0].replace("0","") : eventDate.split(" ")[0];
        String monthValue = eventDate.split(" ")[1];
        String yearValue = eventDate.split(" ")[2];

        if(!yearValue.equals(monthAndYearButton.get(1).getText())){
            monthAndYearButton.get(1).click();
            Thread.sleep(THREE_HUNDRED_MS);
            for(WebElement calendarCell : calendarCells){
                if(yearValue.equals(calendarCell.getText())){
                    calendarCell.click();
                    break;
                } else{
                    cellsIdx++;
                }
            }
            if(cellsIdx==calendarCells.size()){
                return false;
            }

            cellsIdx = 0;
            for(WebElement calendarCell : calendarCells){
                if(monthValue.contains(calendarCell.getText())) {
                    calendarCell.click();
                    break;
                } else{
                    cellsIdx++;
                }
            }
            if(cellsIdx==calendarCells.size()){
                return false;
            }
        }else if(!monthValue.contains(monthAndYearButton.get(0).getText())){
            monthAndYearButton.get(0).click();
            Thread.sleep(THREE_HUNDRED_MS);

            cellsIdx = 0;
            for(WebElement calendarCell : calendarCells){
                if(monthValue.contains(calendarCell.getText())) {
                    calendarCell.click();
                    break;
                } else{
                    cellsIdx++;
                }
            }
            if(cellsIdx==calendarCells.size()){
                return false;
            }
        }

        cellsIdx = 0;
        for(WebElement calendarCell : calendarCells){
            if(calendarCell.getText().equals(dayValue)) {
                calendarCell.click();
                break;
            } else{
                cellsIdx++;
            }
        }
        return cellsIdx != calendarCells.size();
    }

    public void cancelAlertOfNotification(){
        alertCancelButton.click();
    }


    public String getResourceFileAbsolutePathFromFileName(String requiredFileName){

        String eventImagePath = IMAGES_RESOURCES_FOLDER_NAME + requiredFileName;

        return ClassLoader.getSystemResource(eventImagePath).toString()
                .replace(PART_TO_REMOVE_FROM_GENERATED_FILE_NAME,EMPTY).replaceAll("/","\\\\");
    }

}
