package screens;

import constants.ConstantParameters;
import org.openqa.selenium.*;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

import java.util.List;

public class HomeScreen extends BaseScreen{

    @FindBy(className = HOME_SCREEN_COMPANY_NAME_CLASS)
    private WebElement homeScreenCompanyName;

    @FindBy(className = HOME_SCREEN_INSTITUTES_CLASS)
    private List<WebElement> allInstitutesInHomePage;

    @FindBy(className = HOME_SCREEN_CALENDAR_TABLE_CLASS)
    private WebElement calendarTableSection;

    @FindBy(id = HOME_SCREEN_CALENDAR_TAB_ID)
    private WebElement homeScreenCalendarTab;

    @FindBy(id = HOME_SCREEN_CREATE_EVENT_BUTTON_ID)
    private WebElement createEventButton;

    @FindBy(id = CREATE_EVENT_SCREEN_EVENT_TITLE_TEXT_FIELD_ID)
    private WebElement eventTitleTextField;

    public HomeScreen(WebDriver driver) {
        super(driver);
    }

    public boolean navigateToRequiredInstitute(String requiredInstituteName) throws InterruptedException {
        int instituteIdx = 0;
        int instituteSizeAtSearchForRequiredOne = allInstitutesInHomePage.size();
        for (WebElement institute : allInstitutesInHomePage) {
            if(institute.getText().equals(requiredInstituteName)){
                institute.click();
                break;
            } else {
                instituteIdx++;
            }
        }
        if(instituteIdx < instituteSizeAtSearchForRequiredOne){
            try {
                webDriverWait.until(ExpectedConditions.visibilityOf(homeScreenCalendarTab));
                Thread.sleep(THREE_HUNDRED_MS);
                return true;
            } catch (TimeoutException timeoutException){
                return false;
            }
        }else{
            return false;
        }
    }

    public boolean navigateToCalendarTab() throws InterruptedException {
        homeScreenCalendarTab.click();
        return this.waitCalendarTabToLoad();
    }

    public boolean navigateToCreateEvent() throws InterruptedException {
        createEventButton.click();
        return this.waitCreateEventTabToLoad();
    }

    public boolean waitCalendarTabToLoad() throws InterruptedException {
        try {
            webDriverWait.until(ExpectedConditions.visibilityOf(calendarTableSection));
            Thread.sleep(ConstantParameters.THREE_HUNDRED_MS);
            return true;

        } catch (TimeoutException timeoutException){
            return false;
        }

    }

    public boolean waitCreateEventTabToLoad() throws InterruptedException {
        try {
            webDriverWait.until(ExpectedConditions.visibilityOf(eventTitleTextField));
            Thread.sleep(ConstantParameters.THREE_HUNDRED_MS);
            return true;

        } catch (TimeoutException timeoutException){
            return false;
        }

    }


}
