package screens;

import constants.ConstantParameters;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class LandingScreen extends BaseScreen{

    @FindBy(id = LOGIN_SCREEN_EMAIL_ID)
    private WebElement loginEmailTextField;

    public LandingScreen(WebDriver driver) {
        super(driver);
    }

    public boolean waitLoginScreenToLoad() throws InterruptedException {
        try {
            webDriverWait.until(ExpectedConditions.visibilityOf(loginEmailTextField));
            Thread.sleep(ConstantParameters.THREE_HUNDRED_MS);
            return true;
        } catch (TimeoutException timeoutException){
            return false;
        }

    }
}
