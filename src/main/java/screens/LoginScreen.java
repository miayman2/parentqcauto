package screens;

import constants.ConstantParameters;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.ui.ExpectedConditions;

public class LoginScreen extends BaseScreen{

    @FindBy(id = LOGIN_SCREEN_EMAIL_ID)
    private WebElement loginEmailTextField;

    @FindBy(id = LOGIN_SCREEN_PASSWORD_ID)
    private WebElement loginPasswordTextField;

    @FindBy(id = LOGIN_SCREEN_SUBMIT_BUTTON_ID)
    private WebElement submitButton;

    @FindBy(className = HOME_SCREEN_COMPANY_NAME_CLASS)
    private WebElement homeScreenCompanyName;

    public LoginScreen(WebDriver driver) {
        super(driver);
    }

    public void setupLoginEmail(String emailValue){
        loginEmailTextField.clear();
        loginEmailTextField.sendKeys(emailValue);
    }

    public void setupLoginPassword(String passwordValue){
        loginPasswordTextField.clear();
        loginPasswordTextField.sendKeys(passwordValue);
    }

    public boolean submitLoginRequest() throws InterruptedException {
        submitButton.click();
        return this.waitHomeScreenToLoad();
    }

    public boolean waitHomeScreenToLoad() throws InterruptedException {
        try {
            webDriverWait.until(ExpectedConditions.visibilityOf(homeScreenCompanyName));
            Thread.sleep(ConstantParameters.THREE_HUNDRED_MS);
            return true;
        } catch (TimeoutException timeoutException){
            return false;
        }

    }

}
