package data;

public enum UserData {

    DEMO_USER("demo@parent.eu", "12345678", "Kids Palace");

    public String email;
    public String password;
    public String instituteToTest;


    UserData(String email, String password, String instituteToTest){
        this.email = email;
        this.password = password;
        this.instituteToTest = instituteToTest;
    }
}
