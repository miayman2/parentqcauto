package data;

import java.util.ArrayList;
import java.util.Arrays;

public enum EventData {

    VALID_EVENT("Ayman Mansour Event", "this event is the automated QC sample",
            new String[]{"1 Ants Class (Children only)","Kids Palace (Children and staff)"},
            "17 March 2021", "09:00 AM", "06:00 PM", true,
            "Children Only", "15 March 2021",
            true, false, true),

    VALID_EVENT_MANDATORY_ONLY("Ayman Mansour Event", "",
                        new String[]{"1 Ants Class (Children only)","Kids Palace (Children and staff)"},
            "17 March 2021", "09:00 AM", "06:00 PM", false,
            "", "",
            false, false, false);



    public String eventTitle;
    public String eventDescription;
    public ArrayList<String> eventRecipients = new ArrayList<>();
    public String eventDate;
    public String eventTimeStart;
    public String eventTimeEnd;
    public boolean isReservationNeeded;
    public String eventFor;
    public String replyDeadline;
    public boolean isReminderNeeded;
    public boolean isZoomMeetingNeeded;
    public boolean isAddPic;


    EventData(String eventTitle, String eventDescription, String[] eventRecipients,
              String eventDate, String eventTimeStart, String eventTimeEnd,
              boolean isReservationNeeded, String eventFor, String replyDeadline,
              boolean isReminderNeeded,
              boolean isZoomMeetingNeeded, boolean isAddPic){

        this.eventTitle = eventTitle;
        this.eventDescription = eventDescription;
        this.eventRecipients.addAll(Arrays.asList(eventRecipients));
        this.eventDescription = eventDescription;
        this.eventDate = eventDate;
        this.eventTimeStart = eventTimeStart;
        this.eventTimeEnd = eventTimeEnd;
        this.isReservationNeeded = isReservationNeeded;
        this.eventFor = eventFor;
        this.replyDeadline = replyDeadline;
        this.isReminderNeeded = isReminderNeeded;
        this.isZoomMeetingNeeded = isZoomMeetingNeeded;
        this.isAddPic = isAddPic;
    }

}
