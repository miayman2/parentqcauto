package utils;

import constants.ConstantParameters;


public class PropertyHandler extends ConstantParameters {

    public static String[] initBeforeSuitePropertiesFromConfig(String driverName){

        String[] resultsToReturn = new String[]{driverName};

        if(driverName == null || driverName.isEmpty())
        {
            resultsToReturn[0] = configValuesObj.getDriverName();
        }

        return resultsToReturn;
    }
}

