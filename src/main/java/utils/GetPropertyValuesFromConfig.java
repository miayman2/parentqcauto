package utils;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

public class GetPropertyValuesFromConfig {

    private Properties propertiesFromConfigFile;
    private InputStream inputStream;
    private String propKey;
    private String propDefaultValue;

    public GetPropertyValuesFromConfig() {
        propertiesFromConfigFile = new Properties();
        String configFileName = "config.properties";

        inputStream = getClass().getClassLoader().getResourceAsStream(configFileName);

        try {
            if (inputStream != null) {
                propertiesFromConfigFile.load(inputStream);
            } else {
                throw new FileNotFoundException("property file '" + configFileName + "' not found in the classpath");
            }
        } catch (IOException e) {
            System.out.println("Configuration file not exist or cannot be accessed: " + e);
        }
    }

    public void closeConfigFileStream(){
        try {
            if (inputStream != null) {
                inputStream.close();
            }
        } catch (IOException e) {
            System.out.println("Configuration file Stream cannot be closed" + e);
        }
    }

    public String getDriverName(){
        propKey = "driverName";
        propDefaultValue = "Chrome";
        return propertiesFromConfigFile.getProperty(propKey, propDefaultValue);
    }
}
