package constants;

import utils.GetPropertyValuesFromConfig;

public class ConstantParameters
{
    protected static final GetPropertyValuesFromConfig configValuesObj = new GetPropertyValuesFromConfig();


    protected static final long ONE_SECONDS_IN_MS = 1000;
    protected static final long THREE_HUNDRED_MS = 300;
    protected static final long FIVE_HUNDRED_MS = 500;
    protected static final long WAIT_TIME_OUT_IN_SECONDS = 60;

    protected static final String CHROME_DRIVER_NAME = "Chrome";
    protected static final String FIREFOX_DRIVER_NAME = "Firefox";

    protected static final String LANDING_SCREEN_URL = "https://portal-staging.parent.cloud/login";

    protected static final String MESSAGE_WHEN_LOGIN_PAGE_NOT_LOADING = "Testcase didn't run, check internet connection and server status, then try again.";
    protected static final String MESSAGE_WHEN_LOGIN_NOT_SUCCESS = "Login request didn't take place, review email and password and try again.";
    protected static final String MESSAGE_WHEN_CALENDAR_SCREEN_NOT_LOADING = "Calendar screen did not load correctly, check internet connection and try again.";
    protected static final String MESSAGE_WHEN_INSTITUTE_NOT_OPEN = "Check InstituteName, and your Internet Connection, then try again.";
    protected static final String MESSAGE_WHEN_CREATE_EVENT_NOT_OPEN = "Check your Internet Connection, then try again.";
    protected static final String MESSAGE_WHEN_RECIPIENT_NOT_FOUND = "one or more recipients not exist, update your data and try again";
    protected static final String MESSAGE_WHEN_EVENT_FOR_NOT_FOUND = "your selected option not exist anymore, update your data and try again";
    protected static final String MESSAGE_WHEN_DATE_NOT_VALID = "your Date either in the past or far in the future, update your Data and Try again";


    protected static final String MORNING_PART_IN_TIME_FIELDS = " AM";
    protected static final String EVENING_PART_IN_TIME_FIELDS = " PM";
    protected static final String START_OF_STUFF_CHILDREN_PART_RECIPIENTS_FIELDS = " \\(";


    protected static final String SUCCESSFULLY_CREATED_EVENT_TOASTER_MESSAGE = "Calendar event registered";


    protected static final String PART_TO_REMOVE_FROM_GENERATED_FILE_NAME = "file:/";
    protected static final String IMAGES_RESOURCES_FOLDER_NAME = "imagesData/";
    protected static final String EVENT_IMAGE_FILE_NAME = "FunEvents.jpg";

    protected static final String INPUT_ELEMENT_TAG_NAME = "input";
    protected static final String WEB_ELEMENT_CLASS_NAME = "class";

    protected static final String CHECKED_ATTRIBUTE_NAME = "checked";

    protected static final String EMPTY = "";

    protected static final String IS_ACTION_SUCCESS_KEY = "isActionSuccess";
    protected static final String ERROR_MESSAGE_KEY = "errorMessage";

}
