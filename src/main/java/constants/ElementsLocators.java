package constants;

public class ElementsLocators extends ConstantParameters {

    protected static final String LOGIN_SCREEN_EMAIL_ID = "txtEmail";
    protected static final String LOGIN_SCREEN_PASSWORD_ID = "txtPassword";
    protected static final String LOGIN_SCREEN_SUBMIT_BUTTON_ID = "submitBtn";


    protected static final String HOME_SCREEN_COMPANY_NAME_CLASS = "company-name";
    protected static final String HOME_SCREEN_INSTITUTES_CLASS = "institution__name";
    protected static final String HOME_SCREEN_CALENDAR_TAB_ID = "calendarTab";
    protected static final String HOME_SCREEN_CREATE_EVENT_BUTTON_ID = "createEventBtn";
    protected static final String HOME_SCREEN_CALENDAR_TABLE_CLASS = "calendar__table";
    protected static final String HOME_SCREEN_CREATE_EVENT_SUCCESS_TOASTER = "toast-container";



    protected static final String CREATE_EVENT_SCREEN_EVENT_TITLE_TEXT_FIELD_ID = "eventTitle";
    protected static final String CREATE_EVENT_SCREEN_EVENT_DESCRIPTION_TEXT_AREA_ID = "eventDescription";
    protected static final String CREATE_EVENT_SCREEN_EVENT_START_TIME_FIELD_ID = "timepickerStartTime";
    protected static final String CREATE_EVENT_SCREEN_EVENT_END_TIME_FIELD_ID = "timepickerEndTime";
    protected static final String CREATE_EVENT_SCREEN_RESERVATION_NEEDED_BUTTON_ID = "is_reservation_needed";
    protected static final String CREATE_EVENT_SCREEN_REMIND_ME_BEFORE_BUTTON_ID = "reminder_before_event";
    protected static final String CREATE_EVENT_SCREEN_EVENT_FOR_ID = "event_for";
    protected static final String CREATE_EVENT_SCREEN_EVENT_DATE_FIELD_CSS = "[class=\"p-relative input_form clickable\"]";
    protected static final String CREATE_EVENT_SCREEN_RECIPIENTS_FIELD_CSS = "[ng-reflect-placeholder=\"Search for recipients\"]";
    protected static final String CREATE_EVENT_SCREEN_LIST_OPTIONS_CSS = "[role=\"option\"]";
    protected static final String CREATE_EVENT_SCREEN_REPLY_DEADLINE_FIELD_ID = "reply_deadline";
    protected static final String CREATE_EVENT_SCREEN_DATE_MONTH_AND_YEAR_CLASS = "current";
    protected static final String CREATE_EVENT_SCREEN_DATE_CALENDAR_PICKER_CLASS = "bs-datepicker";
    protected static final String CREATE_EVENT_SCREEN_DATE_CALENDAR_CELL_CSS = "[role=\"gridcell\"]";
    protected static final String CREATE_EVENT_SCREEN_UPLOAD_IMAGE_BUTTON = "[class=\"btn btn-primary dropdown-toggle f-sm f-bold\"]";
    protected static final String CREATE_EVENT_SCREEN_UPLOAD_IMAGE_INPUT_FIELD = "imageInput";
    protected static final String CREATE_EVENT_SCREEN_UPLOADED_IMAGE_PREVIEW_ID = "attachment_0";
    protected static final String CREATE_EVENT_SCREEN_SUBMIT_EVENT_BUTTON_ID = "submitCreateEventBtn";
}
