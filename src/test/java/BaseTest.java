import constants.ConstantParameters;
import org.junit.Assert;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.*;
import screens.LandingScreen;
import utils.PropertyHandler;

public class BaseTest extends ConstantParameters {

    protected WebDriver driver;
    LandingScreen landingScreen;
    protected String usedBrowserName = CHROME_DRIVER_NAME;

    @Parameters({"driverName"})
    @BeforeSuite
    public void prerequisitesSuite(@Optional String driverName) {

        driverName = PropertyHandler.initBeforeSuitePropertiesFromConfig(driverName)[0];

        if (!FIREFOX_DRIVER_NAME.equals(driverName)) {
            ChromeOptions chromeDriverOptions = new ChromeOptions();
            chromeDriverOptions.addArguments("--start-maximized");
            chromeDriverOptions.addArguments("--disable-popup-blocking");
            driver = new ChromeDriver(chromeDriverOptions);
        } else {
            usedBrowserName = FIREFOX_DRIVER_NAME;
            driver = new FirefoxDriver();
            driver.manage().window().maximize();
        }
    }

    @BeforeMethod
    public void setupTestCase() throws InterruptedException {
        landingScreen = new LandingScreen(driver);
        landingScreen.navigateToPortal();
        Assert.assertTrue(MESSAGE_WHEN_LOGIN_PAGE_NOT_LOADING,
                landingScreen.waitLoginScreenToLoad());
    }

    @AfterMethod(alwaysRun = true)
    public void tearDownAfterFinishCase(){
        landingScreen.jsDriver.executeScript("window.localStorage.clear();");
    }

    @AfterSuite(alwaysRun = true)
    public void tearDownAfterFinishSuite(){

        if(driver!=null){
            driver.quit();
        }
        configValuesObj.closeConfigFileStream();
    }

}
