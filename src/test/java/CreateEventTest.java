import data.EventData;
import data.UserData;
import org.junit.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import userActions.CreateEventActions;
import userActions.UserLoginActions;

import java.util.HashMap;

public class CreateEventTest extends BaseTest {

    private boolean isFullCreateEvent;
    private UserLoginActions userLoginActions;
    private CreateEventActions createEventActions;
    private HashMap<String, Object> assertionMap;

    @BeforeMethod
    public void initializeTestCasesForCreateEvent() throws InterruptedException {
        userLoginActions = new UserLoginActions(driver);
        createEventActions = new CreateEventActions(driver);
        isFullCreateEvent = false;

        Assert.assertTrue(MESSAGE_WHEN_LOGIN_NOT_SUCCESS,
                userLoginActions.login(UserData.DEMO_USER.email, UserData.DEMO_USER.password,
                        usedBrowserName,false));

        assertionMap =
                createEventActions.navigateToCreateEventForm(UserData.DEMO_USER.instituteToTest);

        Assert.assertTrue(
                assertionMap.get(ERROR_MESSAGE_KEY).toString(),
                Boolean.parseBoolean(assertionMap.get(IS_ACTION_SUCCESS_KEY).toString()));
    }

    @Test
    public void SuccessfullyCreateEventWithAllValidData() throws InterruptedException {

        isFullCreateEvent = true;

        assertionMap = createEventActions.fillCreateEventForm(EventData.VALID_EVENT, false);

        Assert.assertTrue(
                assertionMap.get(ERROR_MESSAGE_KEY).toString(),
                Boolean.parseBoolean(assertionMap.get(IS_ACTION_SUCCESS_KEY).toString()));

        Assert.assertEquals(SUCCESSFULLY_CREATED_EVENT_TOASTER_MESSAGE,
                createEventActions.submitCreatedEvent(true));
    }

    @Test
    public void SuccessfullyCreateEventWithMandatoryValidDataOnly() throws InterruptedException {

        isFullCreateEvent = true;

        assertionMap = createEventActions.fillCreateEventForm(EventData.VALID_EVENT_MANDATORY_ONLY, true);

        Assert.assertTrue(
                assertionMap.get(ERROR_MESSAGE_KEY).toString(),
                Boolean.parseBoolean(assertionMap.get(IS_ACTION_SUCCESS_KEY).toString()));

        Assert.assertEquals(SUCCESSFULLY_CREATED_EVENT_TOASTER_MESSAGE,
                createEventActions.submitCreatedEvent(true));
    }

    @AfterMethod(alwaysRun = true)
    public void resetCreatedEventData(){
        if(isFullCreateEvent){
            //TODO should have some API call to delete the created event by its ID, createdDate and/or eventTitle
        }
    }

}