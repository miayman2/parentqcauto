How to Execute TCs
1. Load maven changes in pom.xml.
2. insert chromedriver.exe and geckodriver.exe in the project main folder.
3. run the overall xml suite, or run testcase by testcase from test folder.


NOTES:
1. Use RunAllClassesWithAllTestsTestngSuit.xml to run all testcases in the project
2. Default browser is chrome, if firefox is required, use parameter driverName with value "Firefox"
   in either XML, as a parameter in TestNG Command, or in conf.properties file in resources.
3. At Changing driverName Parameter, TestNG input will have a priority over Conf file input.
4. Download a suitable version(to your OS and browser version) of chromedriver or geckodriver 
   and add them with the same level as src folder
5. If you got testNG error before starting the suite, then please add 
   "-Dtestng.dtd.http=true" to VM options in Run Configuration. 
   Follow answers here: https://stackoverflow.com/questions/57299606/testng-by-default-disables-loading-dtd-from-unsecure-urls